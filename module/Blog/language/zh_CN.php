<?php
return [
	'Blog Posts' => '博客文章',
	'Add a blog post' => '添加一篇博客文章',
	'Edit blog post' => '编辑博客文章',	
	'Cancel' => '取消',
	'Post Title' => '文章标题',
	'Post title' => '文章标题',
	'Post Text' => '文章文本',
	'Post content' => '文章内容',
	'Post Details' => '文章详情',
	'Write new post' => '写新文章',
	'Insert new post' => '插入新文章',
	'Update post' => '更新文章',	
	'Delete post' => '删除文章',
	'Are you sure you want to delete the following post?' => '你确定想要删除这篇文章？',
];